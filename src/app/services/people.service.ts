import { Injectable } from '@angular/core';
import {delay} from 'rxjs/operators';
import {Observable, of as observableOf} from 'rxjs';
import {PersonModel} from '../models/person-model';
import { Person, PeopleData } from '../models/people';

@Injectable({
  providedIn: 'root'
})
export class PeopleService extends PeopleData {

  private mockPeopleList: Person[] = [
    {firstName: 'John', lastName: 'Doe', age: 21, jobTitle: 'Wanna be Signer'},
    {firstName: 'Jane', lastName: 'Doe', age: 22, jobTitle: 'Signer'},
    {firstName: 'Bob', lastName: 'Barker', age: 80, jobTitle: 'TV Host'},
    {firstName: 'John', lastName: 'Doe', age: 21, jobTitle: 'Wanna be Signer'},
  ];

  getPeople(): Observable<PersonModel[]> {
    // TODO: Finish this implementation using the data from mockPeopleList
    // of(true).pipe(delay(100))
    return observableOf(this.mockPeopleList).pipe(delay(100));
  }
}
