import { Observable } from 'rxjs';

export interface Person {
  firstName: string;
  lastName: string;
  age: number;
  jobTitle: string;
}

export abstract class PeopleData {
  abstract getPeople(): Observable<Person[]>;
}
