import { PeopleService } from './../../services/people.service';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { PersonModel } from 'src/app/models/person-model';

@Component({
  selector: 'app-people-component',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss'],
})
export class PeopleComponent implements OnInit {
  public group: PersonModel[];
  public headers = ['firstName', 'lastName', 'age', 'jobTitle'];
  // TODO: Include the PersonService and get the list of People
  constructor(private peopleService: PeopleService) {}

  ngOnInit() {
    this.listOfPeople();
  }

  listOfPeople(): void {
    /**
     * Author: Ramon J. Yniguez
     * Purpose: Call service - retrieve People Data
     * Date: 09/16/2020
     */
    this.peopleService.getPeople().subscribe(data => this.group = data);
  }
}
