import { PeopleService } from './../../services/people.service';
import { NgModule } from '@angular/core';
import { PeopleComponent } from './people.component';
import { PeopleNgrxModule } from '../people-ngrx/people-ngrx.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [PeopleNgrxModule],
  declarations: [PeopleComponent],
  exports: [PeopleComponent],
})
export class PeopleModule {}
