import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  
  public active = false;
  constructor() {}

  ngOnInit() {
  }

  menu(e) {
    /**
     * Author: Ramon J. Yniguez
     * Date: 9/15/202
     * Purpose: Clickable hambermenu, display nav items.
     * Optimization: param value maybe uneccessary 
     */
    if (!e) { return -1; }

    const x = document.getElementById('header-right');
    if (x.style.display === 'block') {
      x.style.display = 'none';
    } else {
      x.style.display = 'block';
    }
  }
}
